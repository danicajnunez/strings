package com.mycompany.strings;


import java.util.Calendar;
import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author nunezd
 */
public class MyProgram {
    
    public static void main(String[] args)
  {
    Calendar calendar = Calendar.getInstance();
    calendar.set(2012, 8, 19);
    Date firstDate = calendar.getTime();
 
    calendar.set(2012, 8, 1);
    Date secondDate = calendar.getTime();
 
    System.out.println("Is firstDate before secondDate? " + firstDate.before(secondDate));
    System.out.println("Is firstDate after secondDate? " + firstDate.after(secondDate));
  }
}
