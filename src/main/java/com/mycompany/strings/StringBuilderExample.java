/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.strings;

import java.text.DateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author nunezd
 */
public class StringBuilderExample {
    
    public static void main(String[] args) {
 
// Using the StringBuilder        
//        String s1 = "Welcome";
//        StringBuilder sb = new StringBuilder(s1);
//        
//        sb.append(" to california");
//        
//        System.out.println(sb);

        String s1 = "Welcome to California";
        System.out.println("Length of String: " + s1.length());
        
        int pos = s1.indexOf("California");
        System.out.println("Position of California" + pos);
        
        String sub = s1.substring(11);
        System.out.println(sub);
        
        String s2 = "Welcome!         ";
            int len1 = s2.length();
            System.out.println(len1);
            String s3 = s2.trim();
            System.out.println(s3.length());
            
        Date d = new Date();
        System.out.println(d);
            
        GregorianCalendar gc = new GregorianCalendar(2009, 1, 28);
        gc.add(GregorianCalendar.DATE, 1); //Adding 1 day to the date
        Date d2 = gc.getTime(); //returns an instance of the date class
        
        DateFormat df = DateFormat.getDateInstance(DateFormat.FULL); //Factory Methods - the "Full" uses more extended formatting
        String sd = df.format(d2);
        System.out.println(sd);
        
         
    }
}

   